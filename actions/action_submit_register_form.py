
# -*- coding: utf-8 -*-
from typing import Dict, Text, Any, List, Union

from rasa_sdk import ActionExecutionRejection
from rasa_sdk import Tracker, Action
from rasa_sdk.events import SlotSet, FollowupAction
from rasa_sdk.executor import CollectingDispatcher
import json
import os
from datetime import datetime
import logging
import re
from lib.db_queries import *
from lib.resume import *

class SubmitRegisterForm(Action):
	"""Example of a custom form action"""

	def name(self):
		# type: () -> Text
		"""Unique identifier of the form"""
		return "action_submit_register_form"

	def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict]:
		try:
			#Get the intent name from the tracker
			intent_name = tracker.latest_message["intent"]["name"]
			#Set is_authenticated to true
			set_slot(tracker.sender_id,"is_authenticated",True)
			#Get the tracker id from the tracker
			tracker_id = tracker.sender_id
			#Update the resume list
			data = resumeForm(sender_id=tracker_id,current_form_name="register_form")
			print(data)
			#Response message for the current form
			response_message = None
			#Resume related slots to set in DB
			db_slots = {}
			db_slots["resume_confirmation"] = data.get("resume_confirmation")
			db_slots["response_message"] = response_message
			db_slots["intent_to_resume"] = data.get("intent_to_resume")
			#Function call to set the slots in DB
			set_multiple_slots(tracker_id,db_slots)
			#Follow up with resume form 
			return [FollowupAction('resume_form')]
		except BaseException as e:
			print(e)
			ssml = "<s>I am sorry! I am still learning and I could not understand what you said. Could you please say that again?</s>"
			fallback_message = '[{ "message": { "template": { "elements": { "title": "I am sorry! I am still learning and I could not understand what you said. Could you please say that again?", "says": "", "visemes": ""},"type": "Card"} } },{"ssml": "<speak>' + ssml + '</speak>"}]'
			dispatcher.utter_message(fallback_message)
			return []