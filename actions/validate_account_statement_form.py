
# -*- coding: utf-8 -*-
from typing import Dict, Text, Any, List, Union

from rasa_sdk import ActionExecutionRejection
from rasa_sdk import Tracker
from rasa_sdk.events import SlotSet, FollowupAction
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormValidationAction
import json
import os
from datetime import datetime
import logging
import re
from lib.db_queries import *

class ValidateAccountStatementForm(FormValidationAction):
	"""Example of a custom form action"""

	def name(self):
		# type: () -> Text
		"""Unique identifier of the form"""
		return "validate_account_statement_form"

	def validate_account_number(
		self,
		value: Text,
		dispatcher: CollectingDispatcher,
		tracker: Tracker,
		domain: Dict[Text, Any],
	):
		account_number = tracker.get_slot("account_number")
		#Set the same slot in DB, against that tracker ID
		set_slot(tracker.sender_id,"account_number",account_number)
		return {"account_number":account_number}

	
	def validate_days(
		self,
		value: Text,
		dispatcher: CollectingDispatcher,
		tracker: Tracker,
		domain: Dict[Text, Any],
	):
		days = tracker.get_slot("days")
		#Set the same slot in DB, against that tracker ID
		set_slot(tracker.sender_id,"days",days)
		return {"days":days}