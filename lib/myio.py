from __future__ import print_function
from __future__ import unicode_literals

import logging
import numpy
import json
import boto3
import inspect
import requests
import requests_async as requests

from rasa.core.channels.rest import RestInput
from rasa.core import utils
from rasa.core import agent
from rasa.core.interpreter import RasaNLUInterpreter
from rasa.core.channels.channel import UserMessage
from rasa.core.channels.channel import CollectingOutputChannel
from rasa.core import utils
import rasa.utils.endpoints
from rasa_sdk.events import SlotSet
from sanic import Sanic, Blueprint, response
from sanic.request import Request
from asyncio import Queue, CancelledError
from flask import jsonify
import asyncio
from unidecode import unidecode
import re
from env import *

logger = logging.getLogger(__name__)


class RasaFramework(RestInput):
	"""A custom http input channel.

	This implementation is the basis for a custom implementation of a chat
	frontend. You can customize this to send messages to Rasa Core and
	retrieve responses from the agent."""

	@classmethod
	def name(cls):
		return "RasaFramework"

	async def _extract_sender(self, req):
		return req.form.get("session", None)

	# noinspection PyMethodMayBeStatic
	def _extract_platform(self, req):
		return req.form.get("platform", None)

	def _extract_group_id(self, req):
		return req.form.get("group_id", None)

	# noinspection PyMethodMayBeStatic
	def _extract_message(self, req):
		return req.form.get("message", None)

	@staticmethod
	async def on_message_wrapper(on_new_message, text, queue, sender_id):
		collector = QueueOutputChannel(queue)

		message = UserMessage(
			text, collector, sender_id, input_channel=RestInput.name()
		)
		await on_new_message(message)

		await queue.put("DONE")

	def stream_response(self, on_new_message, text, sender_id):
		async def stream(resp):
			q = Queue()
			task = asyncio.ensure_future(
				self.on_message_wrapper(on_new_message, text, q, sender_id)
			)
			while True:
				result = await q.get()
				if result == "DONE":
					break
				else:
					await resp.write(json.dumps(result) + "\n")
			await task

		return stream

	def blueprint(self, on_new_message):
		custom_webhook = Blueprint(
			"custom_webhook_{}".format(type(self).__name__),
			inspect.getmodule(self).__name__,
		)

		# noinspection PyUnusedLocal
		@custom_webhook.route("/", methods=["GET"])
		async def health(request: Request):
			return response.json({"status": "ok"})

		@custom_webhook.route("/webhook", methods=["POST"])
		async def receive(request: Request):
			sender_id = await self._extract_sender(request)
			input_channel = self._extract_platform(request)
			text = self._extract_message(request)
			
			should_use_stream = rasa.utils.endpoints.bool_arg(
				request, "stream", default=False
			)

			if should_use_stream:
				return response.stream(
					self.stream_response(on_new_message, text, sender_id),
					content_type="text/event-stream",
				)
			else:
				collector = CollectingOutputChannel()
				# noinspection PyBroadException
				try:
					msg = await on_new_message(
						UserMessage(
							text, collector, sender_id, input_channel=input_channel
						)
					)
					message_received_is = collector.messages[0]
					text = message_received_is.get("text")
					text = json.loads(text)  # convert the string to json
					recipient_id = message_received_is.get("recipient_id")
					
					message = {
						"success": 1,
						"message": text,
						"session": recipient_id
					}
					return response.json(message)

				except CancelledError:
					logger.error(
						"Message handling timed out for "
						"user message '{}'.".format(text)
					)
				except Exception:
					logger.exception(
						"An exception occured while handling "
						"user message '{}'.".format(text)
					)
				return response.json(message)

		return custom_webhook